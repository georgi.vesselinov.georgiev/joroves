package com.company.models.enums;

public enum StatusType {
    ACTIVE,
    FIXED,
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    NOT_DONE,
    IN_PROGRESS,
    DONE
}

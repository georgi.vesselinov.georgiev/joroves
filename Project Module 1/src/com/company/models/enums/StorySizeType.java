package com.company.models.enums;

public enum StorySizeType {
    LARGE,
    MEDIUM,
    SMALL
}

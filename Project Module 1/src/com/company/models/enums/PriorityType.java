package com.company.models.enums;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW
}

package com.company.models.enums;

public enum BugSeverityType {
    CRITICAL,
    MAJOR,
    MINOR
}

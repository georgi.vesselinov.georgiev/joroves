package com.company.models.implementations;

import com.company.models.contracts.Board;
import com.company.models.contracts.Feedback;
import com.company.models.contracts.Team;
import com.company.models.enums.StatusType;

public class FeedbackImpl extends WorkItemImpl implements Feedback {
    private static final String FEEDBACK_STATUS_ALREADY_CHANGED_ERROR_MESSAGE = "Feedback status has already been set to DONE";

    private static final int MIN_RATING = 0;
    private static final int MAX_RATING = 10;
    private static final String INVALID_RATING = "Rating should be between 0 and 10";

    private StatusType feedbackStatusType;
    private int rating;
    private int feedBackForItemID;

    public FeedbackImpl(Team teamAssignedTo, Board boardAssignedTo, int feedBackForItemID
            , String title, String description, int rating) {
        super(teamAssignedTo,boardAssignedTo,title, description);
        this.feedbackStatusType = StatusType.NEW;
        setRating(rating);
        setFeedBackForItemID(feedBackForItemID);
    }

    public void setRating(int rating) {
        if (rating< MIN_RATING ||rating> FeedbackImpl.MAX_RATING){
            throw new IllegalArgumentException(INVALID_RATING);
        }
        this.rating = rating;
    }

    public StatusType getStatusType() {
        return feedbackStatusType;
    }

    public int getRating() {
        return rating;
    }

    public int getFeedBackForItemID() {
        return feedBackForItemID;
    }

    public void setFeedBackForItemID(int feedBackForItemID) {
        this.feedBackForItemID = feedBackForItemID;
    }

    @Override
    public void changeStatus() {
        if (this.feedbackStatusType== StatusType.DONE){
            throw new IllegalArgumentException(FEEDBACK_STATUS_ALREADY_CHANGED_ERROR_MESSAGE);
        }else if (this.feedbackStatusType== StatusType.NEW) {
            this.feedbackStatusType = StatusType.UNSCHEDULED;
        }else if (feedbackStatusType==StatusType.UNSCHEDULED) {
            this.feedbackStatusType = StatusType.SCHEDULED;
        }else if (feedbackStatusType==StatusType.SCHEDULED){
            this.feedbackStatusType = StatusType.DONE;
        }
     }

    @Override
    public String getStatus() {
        return this.feedbackStatusType.toString();
    }

    @Override
    public String toString() {
        return super.toString()+String.format("     status: %s%n     rating: %s%n     related to item: %d%n%n",getStatus(),getRating(),getFeedBackForItemID());
    }
}

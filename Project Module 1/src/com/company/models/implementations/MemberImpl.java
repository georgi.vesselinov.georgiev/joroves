package com.company.models.implementations;

import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.contracts.WorkItem;


import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {
    private static final String INVALID_MEMBER_NAME = "Member name should be between 5 and 15 symbols.";
    private static final int MIN_MEMBER_NAME_LENGTH = 5;
    private static final int MAX_MEMBER_NAME_LENGTH = 15;
    private static final Team unfixed = new TeamImpl("Unfixed");


    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;
    private boolean isMember;
    private Team teamAssignedTo;

    public MemberImpl(String name) {
        setName(name);
        this.workItems = new ArrayList<>();
        this.activityHistory = new ArrayList<>();
        this.isMember=false;
        this.teamAssignedTo=MemberImpl.unfixed;
    }

    private void setName(String name) {
        ValidationHelper.checkMinMax(name,MIN_MEMBER_NAME_LENGTH,MAX_MEMBER_NAME_LENGTH,INVALID_MEMBER_NAME);
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<WorkItem>(workItems);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    public void addActivityHistory(String newActivity) {
        activityHistory.add(newActivity);
    }

    public Team getTeamAssignedTo() {
        return teamAssignedTo;
    }

    public void setTeamAssignedTo(Team teamAssignedTo) {
        this.teamAssignedTo = teamAssignedTo;
    }

    public void setMember(boolean isMember) {
        this.isMember = isMember;
    }

    public boolean isMember() {
        return isMember;
    }


    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void assignWorkItem(WorkItem workItem) {
        this.workItems.add(workItem);
    }

    @Override
    public void unassignWorkItem(WorkItem workItem) {
        this.workItems.remove(workItem);
    }

    public String showPersonsActivity(){
        StringBuilder str = new StringBuilder();
        for (String el:activityHistory){
            str.append(el);
        }
        return str.toString();
    }
}

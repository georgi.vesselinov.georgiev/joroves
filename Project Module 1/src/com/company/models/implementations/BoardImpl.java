package com.company.models.implementations;

import com.company.models.contracts.Board;
import com.company.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final String INVALID_BOARD_NAME = "Board name should be between 5 and 10 symbols.";
    private static final int MIN_BOARD_NAME_LENGTH = 5;
    private static final int MAX_BOARD_NAME_LENGTH = 10;

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        this.workItems = new ArrayList<>();
        this.activityHistory = new ArrayList<>();
    }

    private void setName(String name) {
        ValidationHelper.checkMinMax(name,MIN_BOARD_NAME_LENGTH,MAX_BOARD_NAME_LENGTH,INVALID_BOARD_NAME);
        this.name = name;
    }

    public void addActivityHistory(String newActivity) {
        activityHistory.add(newActivity);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    public void addToBoard(WorkItem workItem){
        workItems.add(workItem);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public String toString() {
        return String.format("%s",name);
    }

    public String showBoardsActivity(){
        StringBuilder str = new StringBuilder();
        for (String el : activityHistory){
            str.append(el);
        }
        return str.toString();

    }
}

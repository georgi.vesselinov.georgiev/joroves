package com.company.models.implementations;

import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSeverityType;
import com.company.models.enums.PriorityType;
import com.company.models.enums.StatusType;



public class BugImpl extends AssignableItemsImpl implements Bug {
    private static final String BUG_STATUS_ALREADY_CHANGED_ERROR_MESSAGE = "Bug status has already been set to FIXED";

    private BugSeverityType bugSeverityType;
    private StatusType bugStatusType;

    public BugImpl(Team teamAssignedTo, Board boardAssignedTo, String title, String description, PriorityType priorityType, BugSeverityType bugSeverityType) {
        super(teamAssignedTo,boardAssignedTo,title, description, priorityType);
        this.bugSeverityType = bugSeverityType;
        this.bugStatusType =StatusType.ACTIVE;
    }

    public BugSeverityType getBugSeverityType() {
        return bugSeverityType;
    }
    public StatusType getStatusType() {
        return bugStatusType;
    }


    @Override
    public void changeBugSeverity(BugSeverityType bugSeverityTypeToBeSet) {
        this.bugSeverityType =bugSeverityTypeToBeSet;
    }

    @Override
    public void changeStatus() {
        if (bugStatusType==StatusType.FIXED){
            throw new IllegalArgumentException(BUG_STATUS_ALREADY_CHANGED_ERROR_MESSAGE);
        }
        this.bugStatusType=StatusType.FIXED;
    }

    @Override
    public String getStatus() {
        return this.bugStatusType.toString();
    }

    @Override
    public String toString() {
        return super.toString()+String.format("     status: %s%n     severity: %s%n%n",getStatusType(),getBugSeverityType());
    }
}

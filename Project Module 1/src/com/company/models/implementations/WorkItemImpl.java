package com.company.models.implementations;


import com.company.models.contracts.Board;
import com.company.models.contracts.Team;
import com.company.models.contracts.WorkItem;
import com.company.models.enums.StatusType;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemImpl implements WorkItem {
    private static final int MIN_WORK_ITEM_NAME_LENGTH = 10;
    private static final int MAX_WORK_ITEM_NAME_LENGTH = 50;
    private static final String INVALID_WORK_ITEM_NAME = "WorkItem name should be between 10 and 50 symbols.";
    private static final int MIN_WORK_ITEM_DESCRIPTION_LENGTH = 10;
    private static final int MAX_WORK_ITEM_DESCRIPTION_LENGTH = 500;
    private static final String INVALID_WORK_ITEM_DESCRIPTION = "WorkItem description should be between 10 and 500 symbols.";
//    private static final Board notFixed = new BoardImpl("NotFixed");
//    private static final Team stillUnfixed = new TeamImpl("StillUnassigned");

    private static int idCounter=0;
    private int workItemID;
    private String title;
    private String description;
    private List<String> comments;
    private List<String> historyOfChanges;
    private Board boardAssignedTo;
    private Team teamAssignedTo;

    public WorkItemImpl(Team teamAssignedTo, Board boardAssignedTo,String title, String description) {

        setTitle(title);
        setDescription(description);
        this.comments = new ArrayList<>();
        this.historyOfChanges = new ArrayList<>();
        this.boardAssignedTo=boardAssignedTo;
        this.teamAssignedTo=teamAssignedTo;
        workItemID = getNextId();
    }

    private void setTitle(String title) {
        ValidationHelper.checkMinMax(title,MIN_WORK_ITEM_NAME_LENGTH,MAX_WORK_ITEM_NAME_LENGTH,INVALID_WORK_ITEM_NAME);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelper.checkMinMax(description,MIN_WORK_ITEM_DESCRIPTION_LENGTH,MAX_WORK_ITEM_DESCRIPTION_LENGTH,INVALID_WORK_ITEM_DESCRIPTION);
        this.description = description; }





    public int getWorkItemID() {
        return workItemID;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public List<String> getComments() {
        return new ArrayList<>(comments);
    }

    public List<String> getHistoryOfChanges() {
        return new ArrayList<>(historyOfChanges);
    }


    @Override
    public void addComment(String comment){
        comments.add(comment);
    }

    @Override
    public void addChangesToHistory (String newChange){
        historyOfChanges.add(newChange);
    }


    @Override
    public abstract void changeStatus();

    public abstract String getStatus();

    public abstract StatusType getStatusType();

    public Board getBoardAssignedTo(){
        return boardAssignedTo;
    }

    public Team getTeamAssignedTo() {
        return teamAssignedTo;
    }

    public String showHistoryOfChanges(){
        StringBuilder strHistory = new StringBuilder();
        for (String el : historyOfChanges){
            strHistory.append(el);
        }
        return strHistory.toString();
    }

    @Override
    public String showComments() {
        StringBuilder strComments = new StringBuilder();
        for (String el : comments){
            strComments.append(el);
            strComments.append(String.format("%n"));
        }
        return strComments.toString();
    }

    @Override
    public String toString() {
        return String.format("workItem ID: %d%n     type: %s%n     assigned to board: %s%n     assigned to team: %s%n     title: %s%n     description: %s%n",
                getWorkItemID(),getWorkItemType(),getBoardAssignedTo(),getTeamAssignedTo().getName(),title,description);
        }

    public String getWorkItemType (){
        return this.getClass().getSimpleName().replace("Impl", "");
    }


    private int getNextId(){
        idCounter++;
        return idCounter;
    }



}

package com.company.models.implementations;

import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeamImpl implements Team {
    private static final String INVALID_TEAM_NAME = "Team name should have more than 0 symbols";

    private String name;
    private List<Member> members;
    private final Map<String, Board> boards;
    private List<String> activityHistory;

    public TeamImpl(String name) {
        setName(name);
        this.members = new ArrayList<>();
        this.boards = new HashMap<>();
        this.activityHistory = new ArrayList<>();
    }

    private void setName(String name) {
        if (name.length()<1){
            throw new IllegalArgumentException(INVALID_TEAM_NAME);
        }
        this.name = name;
    }

    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<Member>(members);
    }

    @Override
    public Map<String, Board> getBoards() {
        return boards;
    }

    @Override
    public void addMember(Member member) {
     members.add(member);
    }

    @Override
    public void addBoard(String name, Board board) {
        this.boards.put(name, board);
    }

    @Override
    public void addActivityHistory(String newActivity) {
        activityHistory.add(newActivity);
    }

    @Override
    public String showTeamMembers() {
        StringBuilder strTeamMembers = new StringBuilder();
        strTeamMembers.append(String.format("Team %s has %d member",getName(),getMembers().size()));
        if (members.size()!=1) {
            strTeamMembers.append("s - ");
        }
        if (members.size()==0){
            strTeamMembers.append(" - ");
        }
        for (Member member : members){
            strTeamMembers.append(member);
            strTeamMembers.append(", ");
        }
        return strTeamMembers.toString();
    }


    @Override
    public String showTeamBoards() {
        StringBuilder strTeamBoards = new StringBuilder();
        strTeamBoards.append(String.format("Team %s has %d board",name,boards.size()));
        if (boards.size()!=1) {
            strTeamBoards.append("s");
        }
        if (boards.size()!=0){
            strTeamBoards.append(" - ");
        }
        for (Board board : boards.values()){
            strTeamBoards.append(board);
            strTeamBoards.append(", ");
        }
        return strTeamBoards.toString();
    }

    @Override
    public String showTeamsActivity(){
        StringBuilder strActivity = new StringBuilder();
        for ( String el : activityHistory){
            strActivity.append(el);
        }
        return strActivity.toString();
    }

    @Override
    public String toString() {
        return String.format("team: %s%n     members: %s%n     boards: %s%n",getName(),showTeamMembers(),showTeamBoards());
    }
}

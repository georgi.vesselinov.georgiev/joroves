package com.company.models.implementations;

import com.company.models.contracts.AssignableItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.PriorityType;

public abstract class AssignableItemsImpl extends WorkItemImpl implements AssignableItems {
    public static final Member unfixed = new MemberImpl("Unfixed");

    private PriorityType priorityType;
    private Member assignee;
    private boolean isAssigned;


    public AssignableItemsImpl(Team teamAssignedTo, Board boardAssignedTo, String title, String description, PriorityType priorityType) {
        super(teamAssignedTo,boardAssignedTo,title, description);
        this.priorityType = priorityType;
        this.assignee=AssignableItemsImpl.unfixed;
        this.isAssigned=false;

    }


    public void setAssignee(Member assignee) {
        this.assignee = assignee;
    }

    public PriorityType getPriorityType() {
        return priorityType;
    }

    public Member getAssignee() {
        return assignee;
    }

    @Override
    public void changePriority(PriorityType priorityTypeToBeSet) {
        this.priorityType=priorityTypeToBeSet;
    }

    public boolean getIsAssigned() {
        return isAssigned;
    }

    public void setIsAssigned(boolean isAssigned) {
        this.isAssigned=isAssigned;
    }

    @Override
    public String toString() {
        return super.toString()+String.format("     assignee: %s%n     priority: %s%n",getAssignee(), getPriorityType());
    }
}

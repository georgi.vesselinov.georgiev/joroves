package com.company.models.implementations;

import java.util.List;

public class ValidationHelper {
    static void checkMinMax(String input, int min, int max, String message) {
        if (input.length()<min||input.length()>max){
            throw new IllegalArgumentException(message);
        }
    }
    public static void ParametersSizeValidation(List<String> parameters,int correctNumberOfArguments, String message) {
        if (parameters.size() != correctNumberOfArguments) {
            throw new IllegalArgumentException(message);
        }
    }

}

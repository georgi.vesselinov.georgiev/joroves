package com.company.models.implementations;

import com.company.models.contracts.Board;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import com.company.models.enums.PriorityType;
import com.company.models.enums.StatusType;
import com.company.models.enums.StorySizeType;


public class StoryImpl extends AssignableItemsImpl implements Story {
    private static final String STORY_STATUS_ALREADY_CHANGED_ERROR_MESSAGE = "Story status has already been set to DONE";
    private static final String STORY_SIZE_TYPE_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE = "Story size with has already been set to %s";

    private StatusType storyStatusType;
    private StorySizeType storySizeType;



    public StoryImpl(Team teamAssignedTo, Board boardAssignedTo, String title, String description, PriorityType priorityType, StorySizeType storySizeType) {
        super(teamAssignedTo,boardAssignedTo,title, description,priorityType);
        this.storyStatusType = StatusType.NOT_DONE;
        this.storySizeType = storySizeType;
    }

    public StatusType getStatusType() {
        return storyStatusType;
    }
    public StorySizeType getStorySizeType() {
        return storySizeType;
    }

    @Override
    public String getStatus() {
        return this.storyStatusType.toString();
    }

    @Override
    public void changeStatus() {
        if (this.storyStatusType== StatusType.DONE){
            throw new IllegalArgumentException(STORY_STATUS_ALREADY_CHANGED_ERROR_MESSAGE);
        }else if (this.storyStatusType== StatusType.NOT_DONE) {
            this.storyStatusType = StatusType.IN_PROGRESS;
        }else if (storyStatusType==StatusType.IN_PROGRESS){
            this.storyStatusType = StatusType.DONE;
        }
    }

    @Override
    public void changeStorySize(StorySizeType storySizeType) {
        if (this.storySizeType== storySizeType){
            throw new IllegalArgumentException(String.format(STORY_SIZE_TYPE_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE,storySizeType));
        }else {
            this.storySizeType = storySizeType;
        }
    }

    @Override
    public String toString() {
        return super.toString()+String.format("     status: %s%n     size: %s%n%n",getStatusType(),getStorySizeType());
    }
}


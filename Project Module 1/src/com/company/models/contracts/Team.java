package com.company.models.contracts;

import java.util.List;
import java.util.Map;

public interface Team {
    String getName();
    List<Member> getMembers();
    Map<String, Board> getBoards();
    void addMember(Member member);
    String showTeamMembers();
    String showTeamBoards();
    void addBoard(String name, Board board);
    String showTeamsActivity();
    void addActivityHistory(String newActivity);
    List<String> getActivityHistory();
}


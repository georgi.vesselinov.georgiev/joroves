package com.company.models.contracts;

import com.company.models.enums.StorySizeType;

public interface Story extends AssignableItems{
    StorySizeType getStorySizeType();
    void changeStorySize (StorySizeType storySizeType);

}

package com.company.models.contracts;

import java.util.List;

public interface Member {
    boolean isMember();
    void setMember(boolean isMember);
    void assignWorkItem(WorkItem workItem);
    void unassignWorkItem(WorkItem workItem);
    String getName();
    List<WorkItem> getWorkItems();
    List<String> getActivityHistory();
    void addActivityHistory(String newActivity);
    String showPersonsActivity();
    void setTeamAssignedTo(Team teamAssignedTo);
    Team getTeamAssignedTo();
    }

package com.company.models.contracts;

public interface Feedback extends WorkItem {
    int getRating();
    void setRating(int rating);
    int getFeedBackForItemID();
    void setFeedBackForItemID(int feedBackForItemID);

}

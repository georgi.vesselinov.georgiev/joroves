package com.company.models.contracts;

import com.company.models.enums.BugSeverityType;


public interface Bug extends AssignableItems {
    BugSeverityType getBugSeverityType();
    void changeBugSeverity(BugSeverityType bugSeverityTypeToBeSet);
}


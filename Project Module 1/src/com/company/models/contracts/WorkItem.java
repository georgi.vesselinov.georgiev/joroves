package com.company.models.contracts;

import com.company.models.enums.StatusType;

import java.util.List;

public interface WorkItem {
     int getWorkItemID();
     String getTitle();
     String getDescription();
     List<String> getComments();
     List<String> getHistoryOfChanges();
     void changeStatus();
     String getStatus();
     StatusType getStatusType();
     void addComment(String comment);
     Board getBoardAssignedTo();
     Team getTeamAssignedTo();
     void addChangesToHistory (String newChange);
     String showHistoryOfChanges();
     String showComments();
     String getWorkItemType ();
}

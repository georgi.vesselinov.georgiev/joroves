package com.company.models.contracts;

import java.util.List;

public interface Board {
    String getName();
    List<WorkItem> getWorkItems();
    List<String> getActivityHistory();
    void addToBoard(WorkItem workItem);
    void addActivityHistory(String newActivity);
    public String showBoardsActivity();
}

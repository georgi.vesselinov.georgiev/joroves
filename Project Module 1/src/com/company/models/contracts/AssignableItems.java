package com.company.models.contracts;

import com.company.models.enums.PriorityType;

public interface AssignableItems extends WorkItem{
    PriorityType getPriorityType();
    Member getAssignee();
    void changePriority(PriorityType priorityTypeToBeSet);
    boolean getIsAssigned();
    void setIsAssigned(boolean isAssigned);
     void setAssignee(Member assignee);

}

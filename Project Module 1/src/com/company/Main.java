package com.company;

import com.company.core.WorkItemsEngineImpl;

public class Main {

    public static void main(String[] args) {
        WorkItemsEngineImpl engine = new WorkItemsEngineImpl();
        engine.start();
    }
}
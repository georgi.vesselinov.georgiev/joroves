package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import com.company.models.implementations.ValidationHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class SortBy implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public SortBy (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);
        String sortBy = parameters.get(0);
        return sortBy(sortBy);
    }

    private String sortBy(String sortBy) {
        if ((!sortBy.equalsIgnoreCase("title"))&&
                (!sortBy.equalsIgnoreCase("priority"))&&
                (!sortBy.equalsIgnoreCase("rating"))&&
                (!sortBy.equalsIgnoreCase("size"))&&
                (!sortBy.equalsIgnoreCase("severity")))
        {
            throw new IllegalArgumentException("Wrong sortBy type input. You can choose between title, priority, severity, size, rating");
        }
        if (sortBy.equalsIgnoreCase("title")) {
            return sortByTitle();
        }
        if (sortBy.equalsIgnoreCase("priority")) {
            return sortByPriority();
        }
        if (sortBy.equalsIgnoreCase("rating")) {
            return sortByRating();
        }
        if (sortBy.equalsIgnoreCase("size")) {
            return sortBySize();
        }
        else {
            return sortBySeverity();
        }
    }


    private  String sortByTitle(){
        List<WorkItem> sortedList =new ArrayList<>(workItemsRepository.getAllItems().values());
        sortedList.sort(Comparator.comparing(WorkItem::getTitle));
        StringBuilder str = new StringBuilder();
        for (WorkItem el:sortedList){
            str.append(el);
        }
        return str.toString();

    }

    private String sortByPriority(){
        List<AssignableItems> sortedAssignableList = new ArrayList<>(workItemsRepository.getAssignableItems().values());

        sortedAssignableList.sort(Comparator.comparing(AssignableItems::getPriorityType));
        StringBuilder str = new StringBuilder();
        for (AssignableItems el:sortedAssignableList){
            str.append(el);
        }
        return str.toString();

    }

    private String sortByRating(){
        List<Feedback> sortedByRatingList = new ArrayList<>(workItemsRepository.getFeedBacks().values());

        Collections.sort(sortedByRatingList, new Comparator<Feedback>() {
            @Override
            public int compare(Feedback o1, Feedback o2) {
                return o1.getRating()-o2.getRating();
            }
        });

//        sortedByRatingList=sortedByRatingList.stream()
//                .sorted((o1, o2) -> o1.getRating()-o2.getRating())
//                .collect(Collectors.toList());

        StringBuilder str = new StringBuilder();
        for (Feedback el:sortedByRatingList){
            str.append(el);
        }
        return str.toString();
    }

    private String sortBySize(){
        List<Story> sortedBySizeList = new ArrayList<>(workItemsRepository.getStories().values());
        Collections.sort(sortedBySizeList,Comparator.comparing(Story::getStorySizeType));
        StringBuilder str = new StringBuilder();
        for (Story el:sortedBySizeList){
            str.append(el);
        }
        return str.toString();
    }

    private String sortBySeverity(){
        List<Bug> sortedBySeverityList = new ArrayList<>(workItemsRepository.getBugs().values());
        Collections.sort(sortedBySeverityList,Comparator.comparing(Bug::getBugSeverityType));
        StringBuilder str = new StringBuilder();
        for (Bug el:sortedBySeverityList){
            str.append(el);
        }
        return str.toString();
    }

}

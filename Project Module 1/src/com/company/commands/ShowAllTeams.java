package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeams implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =0;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowAllTeams (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        return showAllTeams();
    }


    private String showAllTeams(){
        StringBuilder str = new StringBuilder();
        str.append("There ");
        if (workItemsRepository.getTeams().size()==1){
            str.append(String.format("is 1 team:%n%n"));
        }else {
            str.append(String.format("are %d teams:%n%n",workItemsRepository.getTeams().size()));
        }
        workItemsRepository.getTeams().forEach((name,team) -> str.append(team).append(String.format("%n")));
        return str.toString();
    }
}
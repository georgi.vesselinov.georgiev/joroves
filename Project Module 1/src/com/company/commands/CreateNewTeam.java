package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Team;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewTeam (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0);
        return createTeam(teamName);
    }

    private String createTeam(String teamName) {

        if (workItemsRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException (String.format(TEAM_EXISTS_ERROR_MESSAGE, teamName)); }

        Team team = workItemsFactory.createTeam(teamName);
        workItemsRepository.getTeams().put(teamName, team);

        team.addActivityHistory(String.format("Team %s was created;%n",teamName));
        return String.format(TEAM_CREATED_SUCCESS_MESSAGE, teamName);
    }
}

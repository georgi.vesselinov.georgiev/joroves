package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListAllWorkItems implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =0;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ListAllWorkItems (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        return showAllItems();
    }

    public String showAllItems(){
        StringBuilder strAllItems = new StringBuilder();
        strAllItems.append("There ");
        if (workItemsRepository.getAllItems().size()==1){
            strAllItems.append(String.format("is 1 item:%n"));
        }else {
            strAllItems.append(String.format("are %d items:%n%n",workItemsRepository.getAllItems().size()));
        }

        workItemsRepository.getAllItems().forEach((workItemID, workItem)->strAllItems.append(workItem).append(String.format("%n ")));
        return strAllItems.toString();
    }


}

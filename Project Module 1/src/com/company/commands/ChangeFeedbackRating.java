package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ChangeFeedbackRating implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ChangeFeedbackRating (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }
    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        int feedbackRatingToBeSet =Integer.parseInt(parameters.get(1));
        //String person = parameters.get(2);
        return changeFeedbackRating (workItemID,feedbackRatingToBeSet);
    }

    private String changeFeedbackRating(int workItemID,int feedbackRatingToBeSet){
        if (!workItemsRepository.getFeedBacks().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND_ERROR_MESSAGE,"Feedback", workItemID));
        }
        Feedback feedback = workItemsRepository.getFeedBacks().get(workItemID);
        AssignableItems feedBackForItem = workItemsRepository.getAssignableItems().get(feedback.getFeedBackForItemID());
        if (!feedBackForItem.getIsAssigned()){
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_ASSIGNED_ERROR_MESSAGE
                    ,feedback.getFeedBackForItemID()));
        }

        int initialFeedBackRating = feedback.getRating();
        if (feedback.getRating()==feedbackRatingToBeSet){
            throw new IllegalArgumentException(
                    String.format(FEEDBACK_RATING_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE,
                            workItemID,
                            feedbackRatingToBeSet));
        }
        feedback.setRating(feedbackRatingToBeSet);

        Member member = feedBackForItem.getAssignee();
        Board board = feedBackForItem.getBoardAssignedTo();
        Team team = feedBackForItem.getTeamAssignedTo();

        member.addActivityHistory(String.format("Person %s changed the rating of feedback %s from %d to %s; %n"
                ,member.getName(),feedback.getWorkItemID(),initialFeedBackRating,feedbackRatingToBeSet));
        board.addActivityHistory(String.format("The rating of feedback %d was changed from %s to %s by %s;%n"
                ,feedback.getWorkItemID(),initialFeedBackRating,feedbackRatingToBeSet, member.getName()));
        team.addActivityHistory(String.format("The rating of feedback %d was changed from %s to %s by %s;%n"
                ,feedback.getWorkItemID(),initialFeedBackRating,feedbackRatingToBeSet, member.getName()));
        feedback.addChangesToHistory(String.format("The rating of feedback %d was changed from %s to %s by %s;%n"
                ,feedback.getWorkItemID(),initialFeedBackRating,feedbackRatingToBeSet, member.getName()));

        return  String.format(FEEDBACK_RATING_WAS_CHANGED, workItemID, feedbackRatingToBeSet);

    }

}

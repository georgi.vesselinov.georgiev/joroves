package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.AssignableItems;
import com.company.models.contracts.Member;
import com.company.models.contracts.WorkItem;
import com.company.models.implementations.ValidationHelper;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class FilterByStatusOrAssignee implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public FilterByStatusOrAssignee (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters, CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
        String input = parameters.get(0);
        if ((!input.equalsIgnoreCase("ACTIVE"))
                &&(!input.equalsIgnoreCase("FIXED"))
                &&(!input.equalsIgnoreCase("NEW"))
                &&(!input.equalsIgnoreCase("UNSCHEDULED"))
                &&(!input.equalsIgnoreCase("SCHEDULED"))
                &&(!input.equalsIgnoreCase("DONE"))
                &&(!input.equalsIgnoreCase("NOT_DONE"))
                &&(!input.equalsIgnoreCase("IN_PROGRESS"))
                &&(!workItemsRepository.getMembers().containsKey(input))){
            throw new IllegalArgumentException("Wrong input. Choose correct status or person");
        }
        if (workItemsRepository.getMembers().containsKey(input)){
            return filterByAssignee(input);
        }
        return filterByStatus(input);
    }
    private String filterByStatus(String statusType){
        List<WorkItem> filteredList= workItemsRepository.getAllItems().values().stream()
                .filter(workItem -> workItem.getStatus().equalsIgnoreCase(statusType))
                .collect(Collectors.toList());
        StringBuilder strFilteredByStatus = new StringBuilder();
        strFilteredByStatus.append("There ");
        if (filteredList.size()==1){
            strFilteredByStatus.append(String.format("is 1 item with status %s:%n%n",statusType));
        }else {
            strFilteredByStatus.append(String.format("are %d items with statusType %s:%n%n",filteredList.size(),statusType));
        }

        for (WorkItem el : filteredList){
            strFilteredByStatus.append(el);
        }
        return strFilteredByStatus.toString();
    }
    private String filterByAssignee (String name){
        List<AssignableItems> filteredByAssigneeList= workItemsRepository.getAssignableItems().values().stream()
                .filter(workItem -> workItem.getAssignee().getName().equalsIgnoreCase(name))
                .collect(Collectors.toList());
        StringBuilder strFilteredByAssignee = new StringBuilder();
        strFilteredByAssignee.append("There ");
        if (filteredByAssigneeList.size()==1){
            strFilteredByAssignee.append(String.format("is 1 item assigned to %s:%n%n",name));
        }else {
            strFilteredByAssignee.append(String.format("are %d items assigned to %s:%n%n"
                    ,filteredByAssigneeList.size(),name));
        }

        for (WorkItem el : filteredByAssigneeList){
            strFilteredByAssignee.append(el);
        }
        return strFilteredByAssignee.toString();
    }



}

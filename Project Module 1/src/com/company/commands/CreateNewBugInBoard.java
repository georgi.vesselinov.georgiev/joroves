package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSeverityType;
import com.company.models.enums.PriorityType;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewBugInBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =6;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewBugInBoard (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamName =parameters.get(0);
        String boardName =parameters.get(1);
        String bugTitle = parameters.get(2);
        String bugDescription = parameters.get(3);
        PriorityType priorityType = PriorityType.valueOf(parameters.get(4).toUpperCase());
        BugSeverityType bugSeverityType = BugSeverityType.valueOf(parameters.get(5).toUpperCase());


        return createNewBugInBoard(teamName,boardName,bugTitle,bugDescription,
                priorityType,bugSeverityType);
    }

    private String createNewBugInBoard(String teamName,String boardName,String bugTitle,String bugDescription,
                                       PriorityType priorityType,BugSeverityType bugSeverityType){
        // validations
        if (!workItemsRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName));

        }
        Team team = workItemsRepository.getTeams().get(teamName);

        if (!workItemsRepository.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException (String.format(BOARD_NOT_FOUND_ERROR_MESSAGE, boardName));
        }
        Board board = workItemsRepository.getTeams().get(teamName).getBoards().get(boardName);
        Bug bug = workItemsFactory.createBug(team,board,bugTitle,bugDescription,priorityType,bugSeverityType);

        board.addToBoard(bug);
        workItemsRepository.getBugs().put(bug.getWorkItemID(),bug);

//        bug.setTeamAssignedTo(workItemsRepository.getTeams().get(teamName));
//        bug.setBoardAssignedTo(workItemsRepository.getTeams().get(teamName).getBoards().get(boardName));

        team.addActivityHistory(String.format("Bug %d with title %s created in board %s in team%s;%n"
                ,bug.getWorkItemID(),bugTitle,boardName, teamName));
        board.addActivityHistory(String.format("Bug %d with title %s created in board %s in team%s;%n"
                ,bug.getWorkItemID(),bugTitle,boardName, teamName));
        bug.addChangesToHistory(String.format("Bug %d with title %s created in board %s in team%s;%n"
                ,bug.getWorkItemID(),bugTitle,boardName, teamName));

        return String.format(BUG_CREATED_AND_ADDED_IN_BOARD_OF_TEAM, bugTitle, boardName,teamName);
    }
}

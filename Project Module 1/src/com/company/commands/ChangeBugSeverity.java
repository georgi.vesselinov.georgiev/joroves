package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSeverityType;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ChangeBugSeverity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ChangeBugSeverity (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        BugSeverityType bugSeverityTypeToBeSet =BugSeverityType.valueOf(parameters.get(1).toUpperCase());

        return changeBugSeverity(workItemID,bugSeverityTypeToBeSet);
    }

    private String changeBugSeverity(int workItemID,BugSeverityType bugSeverityTypeToBeSet){
        if (!workItemsRepository.getBugs().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE, workItemID));
        }
        Bug bug = workItemsRepository.getBugs().get(workItemID);
        if (bug.getAssignee().getName().equals("Unfixed")){
            throw new IllegalArgumentException(String.format(ITEM_NOT_ASSIGNED_FOUND_ERROR_MESSAGE,"Bug", workItemID));

        }

        String initialSeverity = bug.getBugSeverityType().toString();
        if (bug.getBugSeverityType().equals(bugSeverityTypeToBeSet)){
            throw new IllegalArgumentException(
                    String.format(BUG_SEVERITY_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE,
                            workItemID,
                            bugSeverityTypeToBeSet));
        }

        bug.changeBugSeverity(bugSeverityTypeToBeSet);

        Member member = bug.getAssignee();
        Board board = bug.getBoardAssignedTo();
        Team team = bug.getTeamAssignedTo();

        member.addActivityHistory(String.format("Person %s changed the severity of bug %s from %s to %s; %n"
                ,member.getName(),bug.getWorkItemID(), initialSeverity,bugSeverityTypeToBeSet));
        board.addActivityHistory(String.format("The severity of bug %d was changed from %s to %s by %s;%n"
                ,bug.getWorkItemID(),initialSeverity,bugSeverityTypeToBeSet, member.getName()));
        team.addActivityHistory(String.format("The severity of bug %d was changed from %s to %s by %s;%n"
                ,bug.getWorkItemID(),initialSeverity,bugSeverityTypeToBeSet, member.getName()));
        bug.addChangesToHistory(String.format("The severity of bug %d was changed from %s to %s by %s;%n"
                ,bug.getWorkItemID(),initialSeverity,bugSeverityTypeToBeSet, member.getName()));

        return  String.format(BUG_SEVERITY_WAS_CHANGED, workItemID, bugSeverityTypeToBeSet);
    }

}

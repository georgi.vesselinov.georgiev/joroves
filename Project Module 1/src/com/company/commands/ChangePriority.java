package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.AssignableItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.enums.PriorityType;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ChangePriority implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ChangePriority (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        PriorityType priorityTypeToBeSet =PriorityType.valueOf(parameters.get(1).toUpperCase());

        return changePriority(workItemID,priorityTypeToBeSet);
    }

    private String changePriority(int workItemID,PriorityType priorityTypeToBeSet){
        if (!workItemsRepository.getAssignableItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_ASSIGNABLE_ITEM_ERROR_MESSAGE,workItemID));
        }
        AssignableItems workItem = workItemsRepository.getAssignableItems().get(workItemID);
        if (workItem.getAssignee().getName().equals("Unfixed")){  //?? workitem.getIsassigned
            throw new IllegalArgumentException(String.format(ITEM_NOT_ASSIGNED_FOUND_ERROR_MESSAGE,"Work item ", workItemID));

        }

        String initialPriority = workItem.getPriorityType().toString();
        if (workItem.getPriorityType().equals(priorityTypeToBeSet)){
            throw new IllegalArgumentException(
                    String.format(WORK_ITEM_PRIORITY_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE,
                            workItemID,
                            priorityTypeToBeSet));
        }
        workItem.changePriority(priorityTypeToBeSet);

        Member member = workItem.getAssignee();
        Board board = workItem.getBoardAssignedTo();
        Team team = workItem.getTeamAssignedTo();

        member.addActivityHistory(String.format("Person %s changed the priority of work item %s from %s to %s; %n"
                ,member.getName(),workItem.getWorkItemID(), initialPriority,priorityTypeToBeSet));
        board.addActivityHistory(String.format("The priority of work item %d was changed from %s to %s by %s;%n"
                ,workItem.getWorkItemID(),initialPriority,priorityTypeToBeSet, member.getName()));
        team.addActivityHistory(String.format("The priority of work item %d was changed from %s to %s by %s;%n"
                ,workItem.getWorkItemID(),initialPriority,priorityTypeToBeSet, member.getName()));
        workItem.addChangesToHistory(String.format("The priority of work item %d was changed from %s to %s by %s;%n"
                ,workItem.getWorkItemID(),initialPriority,priorityTypeToBeSet, member.getName()));

        return  String.format(WORK_ITEM_PRIORITY_WAS_CHANGED, workItemID, priorityTypeToBeSet);
    }


}

package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import com.company.models.enums.StorySizeType;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ChangeStorySize implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ChangeStorySize (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        StorySizeType storySizeTypeToBeSet =StorySizeType.valueOf(parameters.get(1).toUpperCase());

        return changeStorySizeType(workItemID,storySizeTypeToBeSet);
    }

    private String changeStorySizeType(int workItemID,StorySizeType storySizeTypeToBeSet){
        if (!workItemsRepository.getStories().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND_ERROR_MESSAGE,"Story", workItemID));
        }
        Story story = workItemsRepository.getStories().get(workItemID);

        String initialStorySize = story.getStorySizeType().toString();
        if (story.getStorySizeType().equals(storySizeTypeToBeSet)){
            throw new IllegalArgumentException(
                    String.format(STORY_SIZE_TYPE_ALREADY_CHANGED_TO_THIS_VALUE_ERROR_MESSAGE,
                            workItemID,
                            storySizeTypeToBeSet));
        }
        if (story.getAssignee().getName().equals("Unfixed")){
            throw new IllegalArgumentException(String.format(ITEM_NOT_ASSIGNED_FOUND_ERROR_MESSAGE,"Story", workItemID));

        }
        story.changeStorySize(storySizeTypeToBeSet);

        Member member = story.getAssignee();
        Board board = story.getBoardAssignedTo();
        Team team = story.getTeamAssignedTo();

        member.addActivityHistory(String.format("The size of story %d was changed from %s to %s by %s;%n"
                ,story.getWorkItemID(),initialStorySize,storySizeTypeToBeSet, member.getName()));
        board.addActivityHistory(String.format("The size of story %d was changed from %s to %s by %s;%n"
                ,story.getWorkItemID(),initialStorySize,storySizeTypeToBeSet, member.getName()));
        team.addActivityHistory(String.format("The size of story %d was changed from %s to %s by %s;%n"
                ,story.getWorkItemID(),initialStorySize,storySizeTypeToBeSet, member.getName()));
        story.addChangesToHistory(String.format("The size of story %d was changed from %s to %s by %s;%n"
                ,story.getWorkItemID(),initialStorySize,storySizeTypeToBeSet, member.getName()));

        return  String.format(STORY_SIZE_WAS_CHANGED, workItemID, storySizeTypeToBeSet);
    }
}

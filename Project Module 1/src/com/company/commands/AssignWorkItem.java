package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.AssignableItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class AssignWorkItem implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public AssignWorkItem(WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        String person = parameters.get(1);
        return assignWorkItem(workItemID, person);
    }

    private String assignWorkItem(int workItemID, String person){
        if (!workItemsRepository.getAssignableItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_ASSIGNABLE_FOUND_ERROR_MESSAGE, workItemID));
        }

        AssignableItems workItem = workItemsRepository.getAssignableItems().get(workItemID);
        Team team = workItem.getTeamAssignedTo();

        if (team.getMembers().stream().noneMatch(member -> member.getName().equals(person))){
            throw new IllegalArgumentException(String.format(PERSON_NOT_RESPONSIBLE_ERROR_MESSAGE, person,workItemID));
        }

        Member member = workItemsRepository.getMembers().get(person);

        if (workItem.getIsAssigned()){
           throw new IllegalArgumentException(String.format(WORK_ITEM_ALREADY_ASSIGNED_ERROR_MESSAGE, workItemID));
        }
        member.assignWorkItem(workItem);
        workItem.setIsAssigned(true);
        workItem.setAssignee(member);

        Board board = workItem.getBoardAssignedTo();

        member.addActivityHistory(String.format("Item %d assigned to person %s;%n",workItemID,person));
        board.addActivityHistory(String.format("Work item %d was assigned to %s;%n",workItemID,person));
        team.addActivityHistory(String.format("Work item %d was assigned to %s;%n",workItemID,person));
        workItem.addChangesToHistory(String.format("Work item %d was assigned to %s;%n",workItemID,person));

        return String.format(ITEM_ASSIGNED_SUCCESS_MESSAGE, workItemID, person);
    }


}

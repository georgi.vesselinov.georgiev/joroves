package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ShowBoardsActivity implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowBoardsActivity (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
        String team = parameters.get(0);
        String boardToShowActivity = parameters.get(1);
        return showBoardsActivity(team,boardToShowActivity);
    }

    private String showBoardsActivity(String team, String boardToShowActivity) {
        if (!workItemsRepository.getTeams().containsKey(team)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, team));
        }
        if (!workItemsRepository.getTeams().get(team).getBoards().containsKey(boardToShowActivity)){
            throw new IllegalArgumentException (String.format(BOARD_NOT_FOUND_ERROR_MESSAGE, boardToShowActivity));

        }

        return workItemsRepository.getTeams().get(team).getBoards().get(boardToShowActivity).showBoardsActivity();
    }
}

package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ChangeStatus implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ChangeStatus (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);
        int workItemID = Integer.parseInt(parameters.get(0));
       // String person = parameters.get(1);
        return changeStatus(workItemID);
    }

    private String changeStatus(int workItemID){
        if (!workItemsRepository.getAllItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE, workItemID));
        }

        int checkForAssigneeID;
        if (workItemsRepository.getAssignableItems().containsKey(workItemID)){
            checkForAssigneeID=workItemID;
        } else{
            checkForAssigneeID=workItemsRepository.getFeedBacks().get(workItemID).getFeedBackForItemID();
        }

        AssignableItems assignableItem =workItemsRepository.getAssignableItems().get(checkForAssigneeID);
        if (!assignableItem.getIsAssigned()){
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_ASSIGNED_ERROR_MESSAGE,checkForAssigneeID));
        }

        WorkItem workItem = workItemsRepository.getAllItems().get(workItemID);
        String initialStatusType = workItem.getStatus();
        workItem.changeStatus();

        Member member = workItemsRepository.getAssignableItems().get(checkForAssigneeID).getAssignee();
        Board board = workItem.getBoardAssignedTo();
        Team team = workItem.getTeamAssignedTo();

        member.addActivityHistory(String.format("Person %s changed the status of work item %s from %s to %s; %n"
                ,member.getName(),workItem.getWorkItemID(),initialStatusType,workItem.getStatus()));
        board.addActivityHistory(String.format("The status of work item %d was changed from %s to %s by %s;%n"
                ,workItemID,initialStatusType,workItem.getStatus(), member.getName()));
        team.addActivityHistory(String.format("The status of work item %d was changed from %s to %s by %s;%n"
                ,workItemID,initialStatusType,workItem.getStatus(), member.getName()));
        workItem.addChangesToHistory(String.format("The status of work item %d was changed from %s to %s by %s;%n"
                ,workItemID,initialStatusType,workItem.getStatus(), member.getName()));

        return  String.format("Status of work item with ID %d was changed to %s", workItemID,workItem.getStatus());
    }
}

package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllPeople implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =0;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowAllPeople (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        return showAllPeople();
    }

    private String showAllPeople(){
        workItemsRepository.getMembers().forEach((name,member) -> System.out.print(name+ ", "));
        return "";
    }

}

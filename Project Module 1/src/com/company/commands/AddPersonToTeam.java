package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class AddPersonToTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public AddPersonToTeam(WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamToAddToName = parameters.get(0);
        String personToBeAddedName = parameters.get(1);
        return addMember(teamToAddToName, personToBeAddedName);
        }

    private String addMember(String teamToAddToName, String personToBeAddedName){
        if (!workItemsRepository.getTeams().containsKey(teamToAddToName)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddToName));
        }

        if (!workItemsRepository.getMembers().containsKey(personToBeAddedName)) {
            throw new IllegalArgumentException(String.format(PERSON_NOT_FOUND_ERROR_MESSAGE, personToBeAddedName));
        }

        Team team = workItemsRepository.getTeams().get(teamToAddToName);
        Member member = workItemsRepository.getMembers().get(personToBeAddedName);
        if (member.isMember()){
            throw new IllegalArgumentException(String.format(PERSON_ALREADY_LISTED_IN_TEAM_ERROR_MESSAGE, personToBeAddedName));
        }
        team.addMember(member);
        member.setMember(true);
        member.addActivityHistory(String.format("Person %s added to team %s; %n",personToBeAddedName,teamToAddToName));
        team.addActivityHistory(String.format("Person %s added to team %s; %n",personToBeAddedName,teamToAddToName));
        member.setTeamAssignedTo(team);

        return String.format(PERSON_ADDED_SUCCESS_MESSAGE, personToBeAddedName, teamToAddToName);
    }
}



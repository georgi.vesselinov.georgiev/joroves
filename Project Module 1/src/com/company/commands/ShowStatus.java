package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.WorkItem;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static com.company.commands.CommandConstants.WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE;

public class ShowStatus implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowStatus (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        return showStatus(workItemID);
    }

    private String showStatus(int workItemID){
        if (!workItemsRepository.getAllItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE,workItemID));
        }
        WorkItem workItem = workItemsRepository.getAllItems().get(workItemID);
        workItem.getStatus();

        return  String.format("Status of work item with ID %d is %s", workItemID,workItem.getStatus());
    }
}

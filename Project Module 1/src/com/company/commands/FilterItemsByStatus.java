package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.WorkItem;
import com.company.models.implementations.ValidationHelper;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class FilterItemsByStatus implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public FilterItemsByStatus (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters, CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
        String itemStatus = parameters.get(0);
        if ((!itemStatus.equalsIgnoreCase("ACTIVE"))
           &&(!itemStatus.equalsIgnoreCase("FIXED"))
                &&(!itemStatus.equalsIgnoreCase("NEW"))
                &&(!itemStatus.equalsIgnoreCase("UNSCHEDULED"))
                &&(!itemStatus.equalsIgnoreCase("SCHEDULED"))
                &&(!itemStatus.equalsIgnoreCase("DONE"))
                &&(!itemStatus.equalsIgnoreCase("NOT_DONE"))
                &&(!itemStatus.equalsIgnoreCase("IN_PROGRESS"))){
            throw new IllegalArgumentException("Wrong item status input. " +
                    "You can choose between active, fixed, new, unscheduled, scheduled, done, not_done, in_progress");
        }
        return filterByStatus(itemStatus);
    }
    private String filterByStatus(String statusType){
        List<WorkItem> filteredList= workItemsRepository.getAllItems().values().stream()
                .filter(workItem -> workItem.getStatus().equalsIgnoreCase(statusType))
                .collect(Collectors.toList());
        StringBuilder strFilteredByStatus = new StringBuilder();
        strFilteredByStatus.append("There ");
        if (filteredList.size()==1){
            strFilteredByStatus.append(String.format("is 1 item with status %s:%n%n",statusType));
        }else {
            strFilteredByStatus.append(String.format("are %d items with statusType %s:%n%n",filteredList.size(),statusType));
        }

        for (WorkItem el : filteredList){
            strFilteredByStatus.append(el);
        }
        return strFilteredByStatus.toString();
    }
}


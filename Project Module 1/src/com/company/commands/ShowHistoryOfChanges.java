package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class ShowHistoryOfChanges implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowHistoryOfChanges (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));

        return showHistoryOfChanges(workItemID);
    }

    private String showHistoryOfChanges(int workItemID) {
        if (!workItemsRepository.getAllItems().containsKey(workItemID)) {
            throw new IllegalArgumentException (String.format(WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE, workItemID));
        }

        return workItemsRepository.getAllItems().get(workItemID).showHistoryOfChanges();
    }
}

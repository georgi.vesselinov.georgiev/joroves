package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.AssignableItems;
import com.company.models.implementations.ValidationHelper;

import java.util.List;
import java.util.stream.Collectors;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class FilterByAssigneeAndStatus implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public FilterByAssigneeAndStatus (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters, CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String status = parameters.get(1);
        if ((!status.equalsIgnoreCase("ACTIVE"))
                &&(!status.equalsIgnoreCase("FIXED"))
                &&(!status.equalsIgnoreCase("NEW"))
                &&(!status.equalsIgnoreCase("UNSCHEDULED"))
                &&(!status.equalsIgnoreCase("SCHEDULED"))
                &&(!status.equalsIgnoreCase("DONE"))
                &&(!status.equalsIgnoreCase("NOT_DONE"))
                &&(!status.equalsIgnoreCase("IN_PROGRESS"))
                &&(!workItemsRepository.getMembers().containsKey(name))){
            throw new IllegalArgumentException("Wrong input. Choose correct status or person");
        }
        return filterByAssigneeAndStatus(name,status);
    }

    private String filterByAssigneeAndStatus (String name, String status){
        List<AssignableItems> filteredByAssigneeAndStatusList= workItemsRepository.getAssignableItems().values().stream()
                .filter(workItem -> workItem.getAssignee().getName().equalsIgnoreCase(name))
                .filter(workItem ->workItem.getStatus().equalsIgnoreCase(status))
                .collect(Collectors.toList());
        StringBuilder strFilteredByAssigneeAndStatus = new StringBuilder();
        strFilteredByAssigneeAndStatus.append("There ");
        if (filteredByAssigneeAndStatusList.size()==1){
            strFilteredByAssigneeAndStatus.append(String.format("is 1 item assigned to %s with status %s:%n%n",name,status));
        }else {
            strFilteredByAssigneeAndStatus.append(String.format("are %d items assigned to %s with status %s:%n%n"
                    ,filteredByAssigneeAndStatusList.size(),name,status));
        }

        for (AssignableItems el : filteredByAssigneeAndStatusList){
            strFilteredByAssigneeAndStatus.append(el);
        }
        return strFilteredByAssigneeAndStatus.toString();
    }
}

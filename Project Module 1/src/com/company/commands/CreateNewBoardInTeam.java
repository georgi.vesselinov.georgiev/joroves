package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Team;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewBoardInTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewBoardInTeam (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0);
        String boardName = parameters.get(1);
        return createBoardInTeam(teamName,boardName);
    }

    private String createBoardInTeam(String teamName,String boardName) {
        if (!workItemsRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName)); }
        if (workItemsRepository.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException (String.format(BOARD_EXISTS_ERROR_MESSAGE, boardName)); }
        Board board = workItemsFactory.createBoard(boardName);
        Team team = workItemsRepository.getTeams().get(teamName);
        team.addBoard(boardName, board);
        board.addActivityHistory(String.format("Board %s created in team %s;%n",board.getName(),team.getName()));
        team.addActivityHistory(String.format("Board %s created in team %s;%n",board.getName(),team.getName()));

        return String.format(BOARD_CREATED_AND_ADDED_IN_TEAM, boardName,teamName);
    }
}

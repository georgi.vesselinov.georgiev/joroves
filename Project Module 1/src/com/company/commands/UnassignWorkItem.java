package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.AssignableItems;
import com.company.models.contracts.Board;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.implementations.AssignableItemsImpl;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class UnassignWorkItem implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public UnassignWorkItem(WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        return unassignWorkItem(workItemID);
    }

    private String unassignWorkItem(int workItemID){
        if (!workItemsRepository.getAssignableItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_ASSIGNABLE_FOUND_ERROR_MESSAGE, workItemID));
        }
        AssignableItems workItem = workItemsRepository.getAssignableItems().get(workItemID);
        if (!workItem.getIsAssigned()){
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_ASSIGNED_ERROR_MESSAGE, workItemID));
        }
        //Member member = workItemsRepository.getMembers().values().stream().filter(member1 ->member1.getWorkItems().contains(workItem)).findFirst().orElse(null);
        Member member =workItem.getAssignee();
        member.unassignWorkItem(workItem);
        workItem.setIsAssigned(false);
        workItem.setAssignee(AssignableItemsImpl.unfixed);

        Board board = workItem.getBoardAssignedTo();
        Team team = workItem.getTeamAssignedTo();

        member.addActivityHistory(String.format("Work item %d was unassigned from person %s;%n",workItemID,member.getName()));
        board.addActivityHistory(String.format("Work item %s was unassigned from person %s;%n",workItemID, member.getName()));
        team.addActivityHistory(String.format("Work item %s was unassigned from person %s;%n",workItemID, member.getName()));
        workItem.addChangesToHistory(String.format("Work item %s was unassigned from person %s;%n",workItemID, member.getName()));

        return String.format(ITEM_UNASSIGNED_SUCCESS_MESSAGE, workItemID, member.getName());
    }
}

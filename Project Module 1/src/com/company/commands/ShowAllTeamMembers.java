package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static com.company.commands.CommandConstants.TEAM_NOT_FOUND_ERROR_MESSAGE;

public class ShowAllTeamMembers implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public ShowAllTeamMembers (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamToShowMembers = parameters.get(0);
        return showTeamMembers(teamToShowMembers);
    }

    private String showTeamMembers(String teamToShowMembers) {
        if (!workItemsRepository.getTeams().containsKey(teamToShowMembers)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToShowMembers));
        }
        return workItemsRepository.getTeams().get(teamToShowMembers).showTeamMembers();
    }
}

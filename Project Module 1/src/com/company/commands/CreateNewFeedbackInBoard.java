package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewFeedbackInBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =6;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewFeedbackInBoard (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamName =parameters.get(0);
        String boardName =parameters.get(1);
        int feedBackForItemID =Integer.parseInt(parameters.get(2));
        String feedbackTitle = parameters.get(3);
        String feedbackDescription = parameters.get(4);
        int rating = Integer.parseInt(parameters.get(5));

        return createNewFeedbackInBoard(teamName,boardName,feedBackForItemID, feedbackTitle,feedbackDescription, rating);
    }

    private String createNewFeedbackInBoard(String teamName,String boardName, int feedBackForItemID, String feedbackTitle,
                                            String feedbackDescription,int rating){
        // validations
        if (!workItemsRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName));
        }
        Team team = workItemsRepository.getTeams().get(teamName);

        if (!workItemsRepository.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException (String.format(BOARD_NOT_FOUND_ERROR_MESSAGE, boardName));
        }
        Board board = workItemsRepository.getTeams().get(teamName).getBoards().get(boardName);

        if (!workItemsRepository.getAssignableItems().containsKey(feedBackForItemID)){
            throw new IllegalArgumentException (String.format(ITEM_FOR_FEEDBACK_NOT_FOUND_ERROR_MESSAGE, feedBackForItemID));
        }
        AssignableItems theObjectOfFeedback =workItemsRepository.getAssignableItems().get(feedBackForItemID);

        if (!workItemsRepository.getTeams().get(teamName).getBoards().get(boardName).getWorkItems().contains(theObjectOfFeedback)){
            throw new IllegalArgumentException (String.format(ITEM_FOR_FEEDBACK_NOT_FOUND_IN_BOARD_ERROR_MESSAGE, feedBackForItemID,boardName));

        }
        if (rating<0||rating>10) {
            throw new IllegalArgumentException (RATING_OUT_OF_SCOPE_ERROR_MESSAGE);
        }

        Feedback feedback = workItemsFactory.createFeedback(team,board,feedBackForItemID,feedbackTitle,feedbackDescription,rating);
        board.addToBoard(feedback);
        workItemsRepository.getFeedBacks().put(feedback.getWorkItemID(),feedback);
        feedback.setFeedBackForItemID(feedBackForItemID);

//        feedback.setTeamAssignedTo(workItemsRepository.getTeams().get(teamName));
//        feedback.setBoardAssignedTo(workItemsRepository.getTeams().get(teamName).getBoards().get(boardName));


        team.addActivityHistory(String.format("Feedback %d with title %s created in board %s in team%s;%n"
                ,feedback.getWorkItemID(),feedbackTitle,boardName, teamName));
        board.addActivityHistory(String.format("Feedback %d with title %s created in board %s in team%s;%n"
                ,feedback.getWorkItemID(),feedbackTitle,boardName, teamName));
        feedback.addChangesToHistory(String.format("Feedback %d with title %s created in board %s in team%s;%n"
                ,feedback.getWorkItemID(),feedbackTitle,boardName, teamName));

        return  String.format(FEEDBACK_CREATED_AND_ADDED_IN_BOARD_OF_TEAM, feedbackTitle, boardName,teamName);

    }
}

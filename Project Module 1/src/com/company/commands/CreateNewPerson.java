package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Member;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewPerson implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewPerson (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String memberName = parameters.get(0);
        return createPerson(memberName);
    }

    private String createPerson(String memberName) {

        if (workItemsRepository.getMembers().containsKey(memberName)) {
            throw new IllegalArgumentException (String.format(PERSON_EXISTS_ERROR_MESSAGE, memberName));
        }

        Member member = workItemsFactory.createMember(memberName);
        workItemsRepository.getMembers().put(memberName, member);
        member.addActivityHistory(String.format("Person %s was created; %n",memberName));

        return String.format(PERSON_CREATED_SUCCESS_MESSAGE, memberName);
    }

}

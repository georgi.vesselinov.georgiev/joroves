package com.company.commands.enums;

public enum CommandType {
    ADD_PERSON_TO_TEAM,
    CREATE_NEW_BOARD_IN_TEAM,
    CREATE_NEW_BUG_IN_BOARD,
    CREATE_NEW_FEEDBACK_IN_BOARD,
    CREATE_NEW_PERSON,
    CREATE_NEW_STORY_IN_BOARD,
    CREATE_NEW_TEAM,
    SHOW_ALL_PEOPLE,
    SHOW_ALL_TEAM_BOARDS,
    SHOW_ALL_TEAM_MEMBERS,
    SHOW_ALL_TEAMS,
    SHOW_BOARDS_ACTIVITY,
    SHOW_PERSONS_ACTIVITY,
    SHOW_TEAMS_ACTIVITY,
    CHANGE_BUG_SEVERITY,
    CHANGE_PRIORITY,
    CHANGE_FEEDBACK_RATING,
    CHANGE_STATUS,
    SHOW_STATUS,
    CHANGE_STORY_SIZE,
    ASSIGN_WORK_ITEM,
    UNASSIGN_WORK_ITEM,
    ADD_COMMENT,
    LIST_ALL_WORK_ITEMS,
    SHOW_HISTORY_OF_CHANGES,
    SHOW_COMMENTS,
    FILTER_ITEMS_BY_TYPE,
    SHOW_ITEM_DETAILS,
    FILTER_ITEMS_BY_STATUS,
    FILTER_ITEMS_BY_STATUS_OR_ASSIGNEE,
    FILTER_ITEMS_BY_ASSIGNEE_AND_STATUS,
    SORT_BY


}

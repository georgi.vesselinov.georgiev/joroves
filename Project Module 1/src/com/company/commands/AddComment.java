package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class AddComment implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =2;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public AddComment(WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        int workItemID = Integer.parseInt(parameters.get(0));
        String comment = parameters.get(1);
        //String author = parameters.get(2);
        return addComment(workItemID, comment);
    }

    private String addComment(int workItemID, String comment){
        if (!workItemsRepository.getAllItems().containsKey(workItemID)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ID_NOT_FOUND_ERROR_MESSAGE, workItemID));
        }

        WorkItem workItem = workItemsRepository.getAllItems().get(workItemID);

        int checkForAssigneeID;
        if (workItemsRepository.getAssignableItems().containsKey(workItemID)){
            checkForAssigneeID=workItemID;
        } else{
            checkForAssigneeID=workItemsRepository.getFeedBacks().get(workItemID).getFeedBackForItemID();
        }

        AssignableItems assignableItem =workItemsRepository.getAssignableItems().get(checkForAssigneeID);
        if (!assignableItem.getIsAssigned()){
            throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_ASSIGNED_ERROR_MESSAGE,checkForAssigneeID));
        }


        Member member = workItemsRepository.getAssignableItems().get(checkForAssigneeID).getAssignee();
        Board board = workItem.getBoardAssignedTo();
        Team team = workItem.getTeamAssignedTo();

        workItem.addComment(comment +" /author: "+member.getName()+"/");

        member.addActivityHistory(String.format("The following comment was added to work item %d by %s: %s%s%s ;%n"
                ,workItemID,member.getName(),"\"",comment,"\"" ));
        board.addActivityHistory(String.format("The following comment was added to work item %d by %s: %s%s%s ;%n"
                ,workItemID,member.getName(),"\"",comment,"\"" ));
        team.addActivityHistory(String.format("The following comment was added to work item %d by %s: %s%s%s ;%n"
                ,workItemID,member.getName(),"\"",comment,"\"" ));
        workItem.addChangesToHistory(String.format("The following comment was added to work item %d by %s: %s%s%s ;%n"
                ,workItemID,member.getName(),"\"",comment,"\"" ));

        return String.format(COMMENT_ADDED_SUCCESS_MESSAGE, workItemID)+ " - "
                +workItemsRepository.getAllItems().get(workItemID).getComments().get(workItemsRepository.getAllItems().get(workItemID).getComments().size()-1);
    }
}

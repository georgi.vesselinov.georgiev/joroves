package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class FilterItemsByType implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =1;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public FilterItemsByType (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters, CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
        String itemType = parameters.get(0);
        if ((!itemType.equalsIgnoreCase("bug")) && (!itemType.equalsIgnoreCase("Story"))
                && (!itemType.equalsIgnoreCase("feedback")) && (!itemType.equalsIgnoreCase("all"))) {
            throw new IllegalArgumentException("Wrong item type input. You can choose between bug, story, feedback or all");

        }
        if (itemType.equalsIgnoreCase("bug")) {
            return showAllBugs();
        }
        if (itemType.equalsIgnoreCase("story")) {
            return showAllStories();
        }
        if (itemType.equalsIgnoreCase("feedback")) {
            return showAllFeedBacks();
        } else {
            return showAllItems();
        }
        //return workItemsRepository.findType(itemType);

    }



    private String showAllItems(){
        StringBuilder strAllItems = new StringBuilder();
        strAllItems.append("There ");
        if (workItemsRepository.getAllItems().size()==1){
            strAllItems.append(String.format("is 1 item:%n"));
        }else {
            strAllItems.append(String.format("are %d items:%n%n",workItemsRepository.getAllItems().size()));
        }

        workItemsRepository.getAllItems().forEach((workItemID, workItem)->strAllItems.append(workItem).append(String.format("%n ")));
        return strAllItems.toString();
    }


    private String showAllBugs(){
        StringBuilder strAllBugs = new StringBuilder();
        strAllBugs.append("There ");
        if (workItemsRepository.getBugs().size()==1){
            strAllBugs.append(String.format("is 1 bug:%n"));
        }else {
            strAllBugs.append(String.format("are %d bugs:%n%n",workItemsRepository.getBugs().size()));
        }

        workItemsRepository.getBugs().forEach((workItemID, workItem)->strAllBugs.append(workItem).append(String.format("%n ")));
        return strAllBugs.toString();
    }

    private String showAllStories(){
        StringBuilder strAllStories = new StringBuilder();
        strAllStories.append("There ");
        if (workItemsRepository.getStories().size()==1){
            strAllStories.append(String.format("is 1 story:%n"));
        }else {
            strAllStories.append(String.format("are %d stories:%n%n",workItemsRepository.getStories().size()));
        }

        workItemsRepository.getStories().forEach((workItemID, workItem)->strAllStories.append(workItem).append(String.format("%n ")));
        return strAllStories.toString();
    }

    public String showAllFeedBacks(){
        StringBuilder strAllFeedbacks = new StringBuilder();
        strAllFeedbacks.append("There ");
        if (workItemsRepository.getFeedBacks().size()==1){
            strAllFeedbacks.append(String.format("is 1 feedback:%n"));
        }else {
            strAllFeedbacks.append(String.format("are %d feedbacks:%n%n",workItemsRepository.getFeedBacks().size()));
        }

        workItemsRepository.getFeedBacks().forEach((workItemID, workItem)->strAllFeedbacks.append(workItem).append(String.format("%n")));
        return strAllFeedbacks.toString();
    }

}

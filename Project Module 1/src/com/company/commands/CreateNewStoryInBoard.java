package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.Board;
import com.company.models.contracts.Story;
import com.company.models.contracts.Team;
import com.company.models.enums.*;
import com.company.models.implementations.ValidationHelper;

import java.util.List;

import static com.company.commands.CommandConstants.*;

public class CreateNewStoryInBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS =6;

    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    public CreateNewStoryInBoard (WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        this.workItemsRepository = workItemsRepository;
        this.workItemsFactory = workItemsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.ParametersSizeValidation(parameters,CORRECT_NUMBER_OF_ARGUMENTS,INVALID_NUMBER_OF_ARGUMENTS);

        String teamName =parameters.get(0);
        String boardName =parameters.get(1);
        String storyTitle = parameters.get(2);
        String storyDescription = parameters.get(3);
        PriorityType priorityType = PriorityType.valueOf(parameters.get(4).toUpperCase());
        StorySizeType storySizeType = StorySizeType.valueOf(parameters.get(5).toUpperCase());

        return createNewStoryInBoard(teamName,boardName,storyTitle,storyDescription,
                priorityType,storySizeType);
    }

    private String createNewStoryInBoard(String teamName, String boardName, String title, String description,
                                         PriorityType priorityType,StorySizeType storySizeType) {

        // validations
        if (!workItemsRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName));
        }

        Team team = workItemsRepository.getTeams().get(teamName);

        if (!workItemsRepository.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException (String.format(BOARD_NOT_FOUND_ERROR_MESSAGE, boardName));
        }
        Board board = workItemsRepository.getTeams().get(teamName).getBoards().get(boardName);

        Story story = workItemsFactory.createStory(team,board,title,description,priorityType,storySizeType);
        board.addToBoard(story);
        workItemsRepository.getStories().put(story.getWorkItemID(),story);

//        story.setTeamAssignedTo(workItemsRepository.getTeams().get(teamName));
//        story.setBoardAssignedTo(workItemsRepository.getTeams().get(teamName).getBoards().get(boardName));


        team.addActivityHistory(String.format("Story %d with title %s created in board %s in team%s;%n"
                ,story.getWorkItemID(),title,boardName, teamName));
        board.addActivityHistory(String.format("Story %d with title %s created in board %s in team%s;%n"
                ,story.getWorkItemID(),title,boardName, teamName));
        story.addChangesToHistory(String.format("Story %d with title %s created in board %s in team%s;%n"
                ,story.getWorkItemID(),title,boardName, teamName));

        return  String.format(STORY_CREATED_AND_ADDED_IN_BOARD_OF_TEAM,story.getWorkItemID(), title, boardName,teamName);
    }
}

package com.company.core;

import com.company.commands.contracts.Command;
import com.company.core.contracts.*;
import com.company.core.factories.CommandFactoryImpl;
import com.company.core.factories.WorkItemsFactoryImpl;
import com.company.core.providers.CommandParserImpl;
import com.company.core.providers.ConsoleReader;
import com.company.core.providers.ConsoleWriter;

import java.util.List;


public class WorkItemsEngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";
    private static final String INVALID_INPUT = "Command cannot be null or empty.";

    private final WorkItemsFactory workItemsFactory;
    private final CommandFactory commandFactory;
    private final CommandParser commandParser;
    private final Reader reader;
    private final Writer writer;

    private final WorkItemsRepository workItemsRepository;

    public WorkItemsEngineImpl() {
        workItemsFactory = new WorkItemsFactoryImpl();
        commandFactory = new CommandFactoryImpl();
        commandParser = new CommandParserImpl();
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        workItemsRepository = new WorkItemsRepositoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String input = reader.readLine();
                if (input.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(input);

            } catch (Exception ex) {
                writer.writeLine(ex.toString());
            }
        }
    }
    private void processCommand(String input) {
        if (input == null || input.trim().equals("")) {
            throw new IllegalArgumentException(INVALID_INPUT);
        }

        String commandTypeAsString = commandParser.parseCommand(input);
        Command command = commandFactory.createCommand(commandTypeAsString, workItemsRepository, workItemsFactory);
        List<String> parameters = commandParser.parseParameters(input);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}

package com.company.core.providers;

import com.company.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.List;

public class CommandParserImpl implements CommandParser {

    @Override
    public String parseCommand(String input) {
        String commandTypeAsString = input.split(", ")[0];
        return commandTypeAsString;
    }

    @Override
    public List<String> parseParameters(String input) {
        String[] commandParts = input.split(", ");
        ArrayList<String> parameters = new ArrayList<String>();
        for (int i = 1; i<commandParts.length;i++){
            parameters.add(commandParts[i]);
        }
        return parameters;
    }
}

package com.company.core.providers;

import com.company.core.contracts.Reader;

import java.util.Scanner;

public class ConsoleReader implements Reader {
    private final Scanner sc;

    public ConsoleReader() {

        sc = new Scanner(System.in);
    }

    public String readLine() {
        return sc.nextLine();
    }

}

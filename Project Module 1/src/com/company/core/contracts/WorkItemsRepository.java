package com.company.core.contracts;

import com.company.models.contracts.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface WorkItemsRepository {
    Map<String, Team> getTeams();
    Map<String, Member> getMembers();
    Map<Integer, Story> getStories();
    Map<Integer, Bug> getBugs();
    Map<Integer, Feedback> getFeedBacks();
    Map <Integer, WorkItem> getAllItems();
    Map <Integer, AssignableItems> getAssignableItems();
}

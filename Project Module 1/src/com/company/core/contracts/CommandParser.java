package com.company.core.contracts;

import java.util.List;

public interface CommandParser {
    String parseCommand(String input);
    List<String> parseParameters(String input);
}

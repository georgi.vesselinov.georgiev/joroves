package com.company.core.contracts;

import com.company.models.contracts.*;
import com.company.models.enums.*;


public interface WorkItemsFactory {

    Team createTeam (String name);

    Member createMember (String name);

    Board createBoard (String name);

    Bug createBug(Team teamAssignedTo, Board boardAssignedTo,String title, String description, PriorityType priorityType,BugSeverityType bugSeverityType);

    Story createStory(Team teamAssignedTo, Board boardAssignedTo,String title, String description, PriorityType priorityType,StorySizeType storySizeType);

    Feedback createFeedback(Team teamAssignedTo, Board boardAssignedTo,int feedBackForItemID, String title, String description, int rating);
}

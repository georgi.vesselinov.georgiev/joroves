package com.company.core.factories;


import com.company.core.contracts.WorkItemsFactory;
import com.company.models.contracts.*;
import com.company.models.enums.*;
import com.company.models.implementations.*;

public class WorkItemsFactoryImpl implements WorkItemsFactory {
    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(Team teamAssignedTo, Board boardAssignedTo,String title, String description, PriorityType priorityType,BugSeverityType bugSeverityType) {
        return new BugImpl(teamAssignedTo,boardAssignedTo,title,description,priorityType,bugSeverityType);
    }

    @Override
    public Story createStory(Team teamAssignedTo, Board boardAssignedTo,String title, String description, PriorityType priorityType,StorySizeType storySizeType) {
        return new StoryImpl(teamAssignedTo,boardAssignedTo,title, description, priorityType, storySizeType);
    }

    @Override
    public Feedback createFeedback(Team teamAssignedTo, Board boardAssignedTo,int feedBackForItemID,String title, String description, int rating) {
        return new FeedbackImpl(teamAssignedTo,boardAssignedTo,feedBackForItemID,title,description,rating);
    }
}

package com.company.core.factories;

import com.company.commands.*;
import com.company.commands.contracts.Command;
import com.company.commands.enums.CommandType;
import com.company.core.contracts.CommandFactory;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, WorkItemsRepository workItemsRepository, WorkItemsFactory workItemsFactory) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType){
            case ADD_PERSON_TO_TEAM:
                return new AddPersonToTeam(workItemsRepository,workItemsFactory);
            case CREATE_NEW_BOARD_IN_TEAM:
                return new CreateNewBoardInTeam(workItemsRepository,workItemsFactory);
            case CREATE_NEW_BUG_IN_BOARD:
                return new CreateNewBugInBoard(workItemsRepository,workItemsFactory);
            case CREATE_NEW_FEEDBACK_IN_BOARD:
                return new CreateNewFeedbackInBoard(workItemsRepository,workItemsFactory);
            case CREATE_NEW_PERSON:
                return new CreateNewPerson(workItemsRepository,workItemsFactory);
            case CREATE_NEW_STORY_IN_BOARD:
                return new CreateNewStoryInBoard(workItemsRepository,workItemsFactory);
            case CREATE_NEW_TEAM:
                return new CreateNewTeam(workItemsRepository,workItemsFactory);
            case SHOW_ALL_PEOPLE:
                return new ShowAllPeople(workItemsRepository,workItemsFactory);
            case SHOW_ALL_TEAM_BOARDS:
                return new ShowAllTeamBoards(workItemsRepository,workItemsFactory);
            case SHOW_ALL_TEAM_MEMBERS:
                return new ShowAllTeamMembers(workItemsRepository,workItemsFactory);
            case SHOW_ALL_TEAMS:
                return new ShowAllTeams(workItemsRepository,workItemsFactory);
            case SHOW_BOARDS_ACTIVITY:
                return new ShowBoardsActivity(workItemsRepository,workItemsFactory);
            case SHOW_PERSONS_ACTIVITY:
                return new ShowPersonsActivity(workItemsRepository,workItemsFactory);
            case SHOW_TEAMS_ACTIVITY:
                return new ShowTeamsActivity(workItemsRepository,workItemsFactory);
            case CHANGE_BUG_SEVERITY:
                return new ChangeBugSeverity(workItemsRepository,workItemsFactory);
            case CHANGE_PRIORITY:
                return new ChangePriority(workItemsRepository,workItemsFactory);
            case CHANGE_FEEDBACK_RATING:
                return new ChangeFeedbackRating(workItemsRepository,workItemsFactory);
            case CHANGE_STATUS:
                return new ChangeStatus(workItemsRepository,workItemsFactory);
            case SHOW_STATUS:
                return new ShowStatus(workItemsRepository,workItemsFactory);
            case CHANGE_STORY_SIZE:
                return new ChangeStorySize(workItemsRepository,workItemsFactory);
            case ASSIGN_WORK_ITEM:
                return new AssignWorkItem(workItemsRepository,workItemsFactory);
            case UNASSIGN_WORK_ITEM:
                return new UnassignWorkItem(workItemsRepository,workItemsFactory);
            case ADD_COMMENT:
                return new AddComment(workItemsRepository,workItemsFactory);
            case LIST_ALL_WORK_ITEMS:
                return new ListAllWorkItems(workItemsRepository,workItemsFactory);
            case SHOW_HISTORY_OF_CHANGES:
                return new ShowHistoryOfChanges(workItemsRepository,workItemsFactory);
            case SHOW_COMMENTS:
                return new ShowComments(workItemsRepository,workItemsFactory);
            case SHOW_ITEM_DETAILS:
                return new ShowItemDetails(workItemsRepository,workItemsFactory);
            case FILTER_ITEMS_BY_TYPE:
                return new FilterItemsByType(workItemsRepository,workItemsFactory);
            case FILTER_ITEMS_BY_STATUS:
                return new FilterItemsByStatus(workItemsRepository,workItemsFactory);
            case FILTER_ITEMS_BY_STATUS_OR_ASSIGNEE:
                return new FilterByStatusOrAssignee(workItemsRepository,workItemsFactory);
            case FILTER_ITEMS_BY_ASSIGNEE_AND_STATUS:
                return new FilterByAssigneeAndStatus(workItemsRepository,workItemsFactory);
            case SORT_BY:
                return new SortBy(workItemsRepository,workItemsFactory);


        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND,commandTypeAsString));

    }
}

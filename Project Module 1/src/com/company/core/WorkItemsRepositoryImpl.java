package com.company.core;

import com.company.core.contracts.WorkItemsRepository;
import com.company.models.contracts.*;
import java.util.*;

public class WorkItemsRepositoryImpl implements WorkItemsRepository {
    private final Map<String, Team> teams;
    private final Map<String, Member> members;
    private final Map<Integer, Story> stories;
    private final Map<Integer, Bug> bugs;
    private final Map<Integer, Feedback> feedBacks;

    public WorkItemsRepositoryImpl() {
        teams = new HashMap<>();
        members = new HashMap<>();
        stories =new HashMap<>();
        bugs = new HashMap<>();
        feedBacks = new HashMap<>();
    }


    @Override
    public Map<String, Team> getTeams() {
        return teams;
    }
    @Override
    public Map<String, Member> getMembers() {return members; }
    @Override
    public Map<Integer, Story> getStories() {
        return stories;
    }
    @Override
    public Map<Integer, Bug> getBugs() {
        return bugs;
    }
    @Override
    public Map<Integer, Feedback> getFeedBacks() {
        return feedBacks;
    }

    public Map <Integer, WorkItem> getAllItems(){
        Map<Integer, WorkItem> result = new HashMap<>();
        result.putAll(stories);
        result.putAll(bugs);
        result.putAll(feedBacks);
        return result;
    }


    public Map<Integer, AssignableItems> getAssignableItems(){
        Map<Integer, AssignableItems> result = new HashMap<>();
        result.putAll(stories);
        result.putAll(bugs);
        return result;
    }


//    public String findType (String type) {
//        List<WorkItem> sortedByType = getAllItems().values().stream()
//                .filter(x -> x.getWorkItemType().equalsIgnoreCase(type))
//                .collect(Collectors.toList());
//        StringBuilder strSortedItems = new StringBuilder();
//        for (WorkItem el :sortedByType){
//            strSortedItems.append(el);
//        }
//        return strSortedItems.toString();
//    }

//    public void sortWorkItems(){
//        getAllItems().values().sort(Comparator.comparing(WorkItem::getPrice).
//                thenComparing(Furniture::getModel));
//    }





}

//
//movies.sort(Comparator.comparing(Movie::getStarred)
//        .reversed()
//        .thenComparing(Comparator.comparing(Movie::getRating)
//        .reversed())
//        );
//        movies.forEach(System.out::println);
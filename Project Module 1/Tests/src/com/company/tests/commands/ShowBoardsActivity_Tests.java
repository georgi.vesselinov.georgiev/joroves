package com.company.tests.commands;

import com.company.commands.ShowBoardsActivity;
import com.company.commands.ShowStatus;
import com.company.commands.contracts.Command;
import com.company.core.WorkItemsRepositoryImpl;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.core.factories.WorkItemsFactoryImpl;
import com.company.models.contracts.Board;
import com.company.models.implementations.BoardImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardsActivity_Tests {
    private Command testCommand;
    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    @Before
    public void before() {
        workItemsRepository = new WorkItemsRepositoryImpl();
        workItemsFactory = new WorkItemsFactoryImpl();
        testCommand = new ShowBoardsActivity(workItemsRepository, workItemsFactory);
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments () {
        // Arrange
        List<String> testList = new ArrayList<>();
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments () {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("board1");
        testList.add("additional");
        // Act & Assert
        testCommand.execute(testList);
    }

}

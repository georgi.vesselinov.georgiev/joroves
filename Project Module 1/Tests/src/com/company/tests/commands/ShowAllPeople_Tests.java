package com.company.tests.commands;

import com.company.commands.ShowAllPeople;
import com.company.commands.ShowAllTeams;
import com.company.commands.contracts.Command;
import com.company.core.WorkItemsRepositoryImpl;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.core.factories.WorkItemsFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllPeople_Tests {
    private Command testCommand;
    private WorkItemsRepository workItemsRepository;
    private WorkItemsFactory workItemsFactory;

    @Before
    public void before(){
        workItemsRepository=new WorkItemsRepositoryImpl();
        workItemsFactory =new WorkItemsFactoryImpl();
        testCommand =new ShowAllPeople(workItemsRepository,workItemsFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("additional");
        // Act & Assert
        testCommand.execute(testList); }


}

package com.company.tests.commands;

import com.company.commands.CreateNewStoryInBoard;
import com.company.commands.contracts.Command;
import com.company.core.WorkItemsRepositoryImpl;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.core.factories.WorkItemsFactoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Team;
import com.company.models.implementations.BoardImpl;
import com.company.models.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewStoryInBoard_Tests {
    private Command testCommand;
    private WorkItemsFactory workItemsFactory;
    private WorkItemsRepository workItemsRepository;

    @Before
    public void before(){
        workItemsRepository = new WorkItemsRepositoryImpl();
        workItemsFactory = new WorkItemsFactoryImpl();
        testCommand=new CreateNewStoryInBoard(workItemsRepository,workItemsFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("team1");
        testList.add("Board1");
        testList.add("titletitletitle");
        testList.add("descriptiondescription");
        testList.add("low");
        // Act & Assert
        testCommand.execute(testList); }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("team1");
        testList.add("Board1");
        testList.add("titletitletitle");
        testList.add("descriptiondescription");
        testList.add("low");
        testList.add("small");
        testList.add("additional");
        // Act & Assert
        testCommand.execute(testList); }

    @Test
    public void execute_should_throwException_when_inputIsValid() {
        // Arrange
        Team testTeam = new TeamImpl("teamName");
        Board testBoard = new BoardImpl("board1");
        workItemsRepository.getTeams().put("teamName",testTeam);
        testTeam.getBoards().put("board1",testBoard);
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("board1");
        testList.add("bugTitlebugTitle");
        testList.add("bugdescriptionaaa");
        testList.add("low");
        testList.add("small");
        // Act & Assert
        testCommand.execute(testList);
        Assert.assertEquals(1,workItemsRepository.getStories().size());

    }
}

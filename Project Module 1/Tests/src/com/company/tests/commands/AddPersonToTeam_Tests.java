package com.company.tests.commands;

import com.company.commands.AddPersonToTeam;
import com.company.commands.contracts.Command;
import com.company.core.WorkItemsRepositoryImpl;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.core.factories.WorkItemsFactoryImpl;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.implementations.MemberImpl;
import com.company.models.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeam_Tests {
    private Command testCommand;
    private WorkItemsFactory workItemsFactory;
    private WorkItemsRepository workItemsRepository;

    @Before
    public void before(){
        workItemsRepository = new WorkItemsRepositoryImpl();
        workItemsFactory = new WorkItemsFactoryImpl();
        testCommand=new AddPersonToTeam(workItemsRepository,workItemsFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("team1");
        // Act & Assert
        testCommand.execute(testList); }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("team1");
        testList.add("gosho");
        testList.add("additional");
        // Act & Assert
        testCommand.execute(testList); }

    @Test
    public void execute_should_throwException_when_inputIsValid() {
        // Arrange
        Team testTeam = new TeamImpl("teamName");
        workItemsRepository.getTeams().put("teamName",testTeam);
        Member testMember = new MemberImpl("gosho");
        workItemsRepository.getMembers().put("gosho",testMember);
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("gosho");
        // Act & Assert
        testCommand.execute(testList);
        Assert.assertEquals(1,testTeam.getMembers().size());
    }
       @Test
    public void execute_should_throwException_when_noActivityHistoryAddedToMember() {
        // Arrange
           Team testTeam = new TeamImpl("teamName");
           workItemsRepository.getTeams().put("teamName",testTeam);
           Member testMember = new MemberImpl("Gosho");
           workItemsRepository.getMembers().put("Gosho",testMember);
           List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("Gosho");
        // Act & Assert
        testCommand.execute(testList);
        Assert.assertEquals(1, workItemsRepository.getMembers().get("Gosho").getActivityHistory().size()); }

        @Test
    public void execute_should_throwException_when_noActivityHistoryAddedToTeam() {
        // Arrange
            Team testTeam = new TeamImpl("teamName");
            workItemsRepository.getTeams().put("teamName",testTeam);
            Member testMember = new MemberImpl("Gosho");
            workItemsRepository.getMembers().put("Gosho",testMember);
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("Gosho");
        // Act & Assert
        testCommand.execute(testList);
        Assert.assertEquals(1, workItemsRepository.getTeams().get("teamName").getActivityHistory().size()); }

}

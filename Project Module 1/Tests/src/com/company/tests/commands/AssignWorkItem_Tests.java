package com.company.tests.commands;

import com.company.commands.AssignWorkItem;
import com.company.commands.contracts.Command;
import com.company.core.WorkItemsRepositoryImpl;
import com.company.core.contracts.WorkItemsFactory;
import com.company.core.contracts.WorkItemsRepository;
import com.company.core.factories.WorkItemsFactoryImpl;
import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Member;
import com.company.models.contracts.Team;
import com.company.models.implementations.BoardImpl;
import com.company.models.implementations.BugImpl;
import com.company.models.implementations.MemberImpl;
import com.company.models.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.company.models.enums.BugSeverityType.MAJOR;
import static com.company.models.enums.PriorityType.LOW;

public class AssignWorkItem_Tests {
    private Command testCommand;
    private WorkItemsFactory workItemsFactory;
    private WorkItemsRepository workItemsRepository;

    @Before
    public void before(){
        workItemsRepository = new WorkItemsRepositoryImpl();
        workItemsFactory = new WorkItemsFactoryImpl();
        testCommand=new AssignWorkItem(workItemsRepository,workItemsFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("1");
        // Act & Assert
        testCommand.execute(testList); }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("1");
        testList.add("gosho");
        testList.add("additional");
        // Act & Assert
        testCommand.execute(testList); }

    @Test
    public void execute_should_throwException_when_inputIsValid() {
        // Arrange
        Team testTeam = new TeamImpl("teamName");
        workItemsRepository.getTeams().put("teamName",testTeam);
        Board testBoard = new BoardImpl("board1");
        testTeam.getBoards().put("board1",testBoard);
        Bug testBug = new BugImpl(testTeam,testBoard,"titletitle","descriptiondescription",LOW,MAJOR);
        workItemsRepository.getBugs().put(1,testBug);
        testBoard.addToBoard(testBug);
        Member testMember = new MemberImpl("gosho");
        workItemsRepository.getMembers().put("gosho",testMember);
        testTeam.addMember(testMember);
        List<String> testList = new ArrayList<>();
        testList.add("1");
        testList.add("gosho");
        // Act & Assert
        testCommand.execute(testList);
        Assert.assertEquals(1,testMember.getWorkItems().size());
    }
}

package com.company.tests.models;


import com.company.models.contracts.Board;
import com.company.models.contracts.Bug;
import com.company.models.contracts.Team;
import com.company.models.enums.BugSeverityType;
import com.company.models.enums.PriorityType;
import com.company.models.implementations.BugImpl;
import org.junit.Before;
import org.junit.Test;

import static com.company.models.enums.BugSeverityType.CRITICAL;
import static com.company.models.enums.PriorityType.LOW;

public class WorkItemImpl_Tests {
    private Team testTeam;
    private Board testBoard;
    private PriorityType testPriorityType;
    private BugSeverityType testBugSeverityType;
    private Bug bug;

    @Before
    public void before() {
        PriorityType testPriorityType = LOW;
        BugSeverityType bugSeverityType = CRITICAL;
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_title_is_smaller_than_minimum() {
        //Arrange, Act, Assert
        Bug bug = new BugImpl(testTeam,testBoard,"bugbugbug","descriptionbug1",LOW,CRITICAL);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_title_is_bigger_than_maximum() {
        //Arrange, Act, Assert
        Bug bug = new BugImpl(testTeam,testBoard,"bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5b","descriptionbug1",LOW,CRITICAL);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_description_is_smaller_than_minimum() {
        //Arrange, Act, Assert
        Bug bug = new BugImpl(testTeam,testBoard,"bugbugbugrrrr","descripti",LOW,CRITICAL);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_description_is_bigger_than_maximum() {
        //Arrange, Act, Assert
        Bug bug = new BugImpl(testTeam,testBoard,"bugbugbugrrrr","bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug5bugbugbug1bugbugbug2bugbugbug3bugbugbug4bugbugbug51",LOW,CRITICAL);
    }
}
